<?php

namespace App\Http\Controllers;

use App\Category;
use App\CateProd;
use App\ChildCate;
use App\ChildProd;
use App\News;
use App\Products;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FlexshopController extends Controller
{
    public function index()
    {
        return view('flexshop.index');
    }

    public function getListProject()
    {
        $htmlCate = "";
        $htmlContent = "";
        $htmlHot = "";
        $getProject = DB::table('news AS ne')
            ->leftjoin('categories AS cat', 'cat.id', '=', 'ne.Cate_id')
            ->select('cat.id as cateID', 'ne.id as newsID', 'cat.name', 'cat.alias AS cateSlug', 'ne.title', 'ne.alias', 'ne.summary', 'ne.description', 'ne.active', 'ne.images', 'ne.created_at')
            ->where('ne.active', 1)
            ->orderBy('newsID', 'DESC')
            ->take(5)
            ->get();
        foreach ($getProject as $project) {
            if (!empty($project)) {
                $htmlContent .= '<div class="box-article-item">';
                $htmlContent .= '<div class="row">';
                $htmlContent .= '<div class="col-xs-12 col-sm-4">';
                $htmlContent .= '<a data-id="' . $project->newsID . '" data-name="tin-tuc" data-title="' . $project->title . '" alt="' . $project->title . '" href="' . 'du-an/' . $project->alias . '">';
                $htmlContent .= '<img src="' . asset('upload/thumbnail/' . $project->images) . '" alt="' . $project->title . '" title="' . $project->title . '" />';
                $htmlContent .= '</a>';
                $htmlContent .= '</div>';
                $htmlContent .= '<div class="col-xs-12 col-sm-8">';
                $htmlContent .= '<h3 class="title-article-inner">';
                $htmlContent .= '<a data-id="206" data-name="tin-tuc" data-title="' . $project->title . '" alt="' . $project->title . '" href="' . route('postProject', [$project->newsID, $project->alias]) . '">' . $project->title . '</a>';
                $htmlContent .= '</h3>';
                $htmlContent .= '<div class="post-detail">';
                $htmlContent .= '<a href="' . route('cateListProject', [$project->cateID, $project->cateSlug]) . '">' . $project->name . '</a> - ' . date('d-m-Y', strtotime($project->created_at)) . '';
                $htmlContent .= '</div>';
                $htmlContent .= '<div class="text-blog">';
                $htmlContent .= '<p>' . $project->description . '</p>';
                $htmlContent .= '</div>';
                $htmlContent .= '</div>';
                $htmlContent .= '</div>';
                $htmlContent .= '</div>';
            }
        }
        //TODO get Category
        $getCate = DB::table('categories')->select('id', 'name', 'alias')->get();
        foreach ($getCate as $cate) {
            if (!empty($cate)) {
                $htmlCate .= '<li class="cat-item"><a href="' . route('cateListProject', [$cate->id, $cate->alias]) . '" data-name="' . $cate->name . '" data-title="' . $cate->name . '">' . $cate->name . '</a></li>';
            }
        }
        //TODO get hot Project
        $getHotProject = DB::table('news AS ne')
            ->select('ne.id', 'ne.title', 'ne.alias', 'ne.hot')
            ->where([
                ['ne.active', 1],
                ['hot', 1]
            ])
            ->orderBy('id', 'DESC')
            ->take(5)
            ->get();
        foreach ($getHotProject as $hot) {
            if (!empty($hot)) {
                $htmlHot .= '<li><a data-id="' . $hot->id . '" data-name="du-an" data-title="' . $hot->title . '" alt="' . $hot->title . '" href="' . route('postProject', [$hot->id, $hot->alias]) . '">' . $hot->title . '</a></li>';
            }
        }
        return view('flexshop.project')->with(['thisListProject' => $htmlContent, 'thisCateHere' => $htmlCate, 'thisHot' => $htmlHot]);
    }

    public function cateProject($id)
    {
        $htmlContent = "";
        $htmlBreadcrumb = "";
        $htmlCate = "";
        $htmlHot = "";
        $getProject = DB::table('news AS ne')
            ->leftjoin('categories AS cat', 'cat.id', '=', 'ne.Cate_id')
            ->select('cat.id as cateID', 'ne.id as newsID', 'cat.name', 'cat.alias AS cateSlug', 'ne.title', 'ne.alias', 'ne.summary', 'ne.description', 'ne.active', 'ne.images', 'ne.created_at')
            ->where([
                ['ne.active', 1],
                ['cat.id', $id]
            ])
            ->orderBy('newsID', 'DESC')
            ->take(5)
            ->get();
        //TODO get breadcrumb
        $getCateName = $getProject->first()->name;
        $htmlBreadcrumb .= '<div class="breadcrumbs">';
        $htmlBreadcrumb .= '<ul class="breadcrumb">';
        $htmlBreadcrumb .= '<li><a href="' . route('homePage') . '" data-name="trang-chu" data-title="Trang chủ"><i class="fa fa-home"></i> Trang chủ</a><span class="divider"></span></li>';
        $htmlBreadcrumb .= '<li><a href="' . route('listProjects') . '" data-name="du-an" data-title="Dự án">Dự án</a><span class="divider"></span></li>';
        $htmlBreadcrumb .= '<li>' . $getCateName . '</li>';
        $htmlBreadcrumb .= '</ul>';
        $htmlBreadcrumb .= '</div>';
        //TODO get Box news
        foreach ($getProject as $project) {
            if (!empty($project)) {
                $htmlContent .= '<div class="box-article-item">';
                $htmlContent .= '<div class="row">';
                $htmlContent .= '<div class="col-xs-12 col-sm-4">';
                $htmlContent .= '<a data-id="' . $project->newsID . '" data-name="tin-tuc" data-title="' . $project->title . '" alt="' . $project->title . '" href="' . 'du-an/' . $project->alias . '">';
                $htmlContent .= '<img src="' . asset('upload/thumbnail/' . $project->images) . '" alt="' . $project->title . '" title="' . $project->title . '" />';
                $htmlContent .= '</a>';
                $htmlContent .= '</div>';
                $htmlContent .= '<div class="col-xs-12 col-sm-8">';
                $htmlContent .= '<h3 class="title-article-inner">';
                $htmlContent .= '<a data-id="206" data-name="tin-tuc" data-title="' . $project->title . '" alt="' . $project->title . '" href="' . route('postProject', [$project->newsID, $project->alias]) . '">' . $project->title . '</a>';
                $htmlContent .= '</h3>';
                $htmlContent .= '<div class="post-detail">';
                $htmlContent .= '<a href="' . route('cateListProject', [$project->cateID, $project->cateSlug]) . '">' . $project->name . '</a> - ' . date('d-m-Y', strtotime($project->created_at)) . '';
                $htmlContent .= '</div>';
                $htmlContent .= '<div class="text-blog">';
                $htmlContent .= '<p>' . $project->description . '</p>';
                $htmlContent .= '</div>';
                $htmlContent .= '</div>';
                $htmlContent .= '</div>';
                $htmlContent .= '</div>';
            }
        }
        //TODO get Category
        $getCate = DB::table('categories')->select('id', 'name', 'alias')->get();
        foreach ($getCate as $cate) {
            if (!empty($cate)) {
                $htmlCate .= '<li class="cat-item"><a href="' . route('cateListProject', [$cate->id, $cate->alias]) . '" data-name="' . $cate->name . '" data-title="' . $cate->name . '">' . $cate->name . '</a></li>';
            }
        }
        //TODO get Hot with id category
        $getHotProject = DB::table('news AS ne')
            ->leftjoin('categories AS cat', 'cat.id', '=', 'ne.Cate_id')
            ->select('cat.id as cateID', 'ne.id as newsID', 'ne.title', 'ne.alias', 'ne.active')
            ->where([
                ['ne.active', 1],
                ['ne.hot', 1],
                ['cat.id', $id]
            ])
            ->orderBy('newsID', 'DESC')
            ->take(5)
            ->get();
        foreach ($getHotProject as $hot) {
            if (!empty($hot)) {
                $htmlHot .= '<li><a data-id="' . $hot->newsID . '" data-name="du-an" data-title="' . $hot->title . '" alt="' . $hot->title . '" href="' . route('postProject', [$hot->newsID, $hot->alias]) . '">' . $hot->title . '</a></li>';
            }
        }
        return view('flexshop.project')->with(['thisListProject' => $htmlContent, 'thisBreadcrumb' => $htmlBreadcrumb, 'thisCateHere' => $htmlCate, 'thisHot' => $htmlHot]);
    }

    public function singleProject($id)
    {
        $htmlContent = "";
        $htmlCate = "";
        $htmlHot = "";
        $htmlBreadcrumb = "";
        $getProject = DB::table('news AS ne')
            ->leftjoin('categories AS cat', 'cat.id', '=', 'ne.Cate_id')
            ->select('ne.id', 'ne.title', 'ne.alias', 'ne.content', 'ne.created_at', 'ne.Cate_id', 'cat.name', 'cat.alias as slugCate')
            ->where([
                ['ne.active', 1],
                ['ne.id', $id]
            ])
            ->first();
        $htmlContent .= '<div class="box-detail">';
        $htmlContent .= '<h3><a>' . $getProject->title . '</a></h3>';
        $htmlContent .= '<div class="post-detail">';
        $htmlContent .= '<a href="' . route('cateListProject', [$getProject->Cate_id, $getProject->slugCate]) . '">' . $getProject->name . '</a> - ' . date('d-m-Y', strtotime($getProject->created_at)) . '</div>';
        $htmlContent .= '<div class="text-blog">';
        $htmlContent .= $getProject->content;
        $htmlContent .= '</div>';
        $htmlContent .= '</div>';
        //TODO get Category
        $getCate = DB::table('categories')->select('id', 'name', 'alias')->get();
        foreach ($getCate as $cate) {
            if (!empty($cate)) {
                $htmlCate .= '<li class="cat-item"><a href="' . route('cateListProject', [$cate->id, $cate->alias]) . '" data-name="' . $cate->name . '" data-title="' . $cate->name . '">' . $cate->name . '</a></li>';
            }
        }
        //TODO get Hot with id category
        $getHotProject = DB::table('news AS ne')
            ->leftjoin('categories AS cat', 'cat.id', '=', 'ne.Cate_id')
            ->select('cat.id as cateID', 'ne.id as newsID', 'ne.title', 'ne.alias', 'ne.active')
            ->where([
                ['ne.active', 1],
                ['ne.hot', 1],
                ['cat.id', $getProject->Cate_id]
            ])
            ->orderBy('newsID', 'DESC')
            ->take(5)
            ->get();
        foreach ($getHotProject as $hot) {
            if (!empty($hot)) {
                $htmlHot .= '<li><a data-id="' . $hot->newsID . '" data-name="du-an" data-title="' . $hot->title . '" alt="' . $hot->title . '" href="' . route('postProject', [$hot->newsID, $hot->alias]) . '">' . $hot->title . '</a></li>';
            }
        }
        //TODO get breadcrumb with variable $getProject
        $htmlBreadcrumb .= '<div class="breadcrumbs">';
        $htmlBreadcrumb .= '<ul class="breadcrumb">';
        $htmlBreadcrumb .= '<li><a href="' . route('homePage') . '" data-name="trang-chu" data-title="Trang chủ"><i class="fa fa-home"></i> Trang chủ</a><span class="divider"></span></li>';
        $htmlBreadcrumb .= '<li><a href="' . route('listProjects') . '" data-name="du-an" data-title="Dự án">Dự án</a><span class="divider"></span></li>';
        $htmlBreadcrumb .= '<li>' . $getProject->title . '</li>';
        $htmlBreadcrumb .= '</ul>';
        $htmlBreadcrumb .= '</div>';
        return view('flexshop.singleProject')->with(['thisPost' => $htmlContent, 'thisCateHere' => $htmlCate, 'thisHot' => $htmlHot, 'thisBreadcrumb' => $htmlBreadcrumb]);
    }

    //TODO here product**************************************
    public function getListProduct()
    {
        $htmlCate = '';
        $htmlProducts = '';
        $htmlBreadcrumb= '';
        //TODO get Category
        $getCate = DB::table('cate_prods as cate')
            ->leftjoin('child_prods as chil', 'cate.id', '=', 'chil.cateParen_id')
            ->select('cate.id as idCate', 'name', 'alias', 'chil.lvl')
            ->where('lvl', 0)
            ->get();
        foreach ($getCate as $cate) {
            $htmlCate .= '<li class="menu_lv1 item-sub-cat"><a data-name="san-pham" data-title="' . $cate->name . '" href="' . route('cateListProduct', [$cate->idCate, $cate->alias]) . '" ><i class="fa fa-angle-double-right" aria-hidden="true"></i> ' . $cate->name . '</a></li>';
        }
        //TODO get Products
        $getData = DB::table('products')
            ->select('id', 'title', 'alias', 'images', 'prices', 'discount', 'hot', 'active')
            ->where([
                ['active', 1],
                ['hot', 1]
            ])
            ->orderBy('id', 'DESC')
            ->take(12)
            ->get();
        foreach ($getData as $data) {
            if (!empty($data)) {
                $htmlProducts .= '<div class="col-xs-12 col-sm-4 col-md-3">';
                $htmlProducts .= '<div  id="product-1003351994" class="item product-inner">';
                $htmlProducts .= '<div class="item-inner transition">';
                if (isset($data->hot)) {
                    $htmlProducts .= '<span class="label label-sale">Sale</span>';
                }
                $htmlProducts .= '<div class="image">';
                $htmlProducts .= '<a class="lt-image" data-id="198" data-name="' . $data->title . '" data-title="' . $data->title . '" alt="' . $data->title . '" href="' . route('postProduct', [$data->id, $data->alias]) . '" >';
                $htmlProducts .= '<img  src="' . asset('upload/thumbnail/' . $data->images) . '" alt="' . $data->title . '"  class="img-1 product-featured-image">';
                $htmlProducts .= '<img  src="' . asset('upload/thumbnail/' . $data->images) . '" alt="' . $data->title . '"  class="img-2 product-featured-image">';
                $htmlProducts .= '</a>';
                $htmlProducts .= '<div class="button-group">';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '<div class="caption">';
                $htmlProducts .= '<h4>';
                $htmlProducts .= '<a data-id="198" data-name="' . $data->title . '" data-title="' . $data->title . '" alt="' . $data->title . '" href="' . route('postProduct', [$data->id, $data->alias]) . '" >';
                $htmlProducts .= $data->title;
                $htmlProducts .= '</a>';
                $htmlProducts .= '</h4>';
                $htmlProducts .= '<p class="price">';
                $htmlProducts .= '<span class="price-old">' . number_format($data->prices) . '₫</span>';
                $htmlProducts .= '<span class="price-new" style="margin-left: 5px">' . number_format($data->discount) . '₫</span>';
                $htmlProducts .= '</p>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
            }
        }
        return view('flexshop.product')->with(['thisCateProduct' => $htmlCate, 'thisProductList' => $htmlProducts]);
    }

    public function cateProduct($id)
    {
        $htmlCate = '';
        $htmlProducts = '';
        $htmlBreadcrumb= '';
        //TODO get Cate
        $getCate = DB::table('cate_prods as cate')
            ->rightjoin('child_prods as chil', 'cate.id', '=', 'chil.cateParen_id')
            ->select('cate.id as idCate', 'cate.name', 'cate.alias', 'cate.weight', 'chil.lvl')
            ->where([
                ['lvl', '<>', 0],
                ['lvl', $id]
            ])
            ->get();
        foreach ($getCate as $cate) {
            $htmlCate .= '<li class="menu_lv1 item-sub-cat"><a data-name="san-pham" data-title="' . $cate->name . '" href="' . route('listProduct', [$cate->idCate, $cate->alias]) . '" ><i class="fa fa-angle-double-right" aria-hidden="true"></i> ' . $cate->name . '</a></li>';
        }

        //TODO get Products
        $getData = DB::table('cate_prods as cate')
            ->rightjoin('child_prods as chil', 'cate.id', '=', 'chil.cateParen_id')
            ->rightjoin('products as prod', 'cate.id', '=', 'prod.Cate_id')
            ->select('prod.id', 'prod.title', 'prod.alias', 'prod.images', 'prod.prices', 'prod.discount')
            ->where([
                ['lvl', $id]
            ])
            ->orderBy('id', 'DESC')
            ->take(12)
            ->get();
        foreach ($getData as $data) {
            if (isset($data) != null) {
                $htmlProducts .= '<div class="col-xs-12 col-sm-4 col-md-3">';
                $htmlProducts .= '<div  id="product-1003351994" class="item product-inner">';
                $htmlProducts .= '<div class="item-inner transition">';
                if (isset($data->hot)) {
                    $htmlProducts .= '<span class="label label-sale">Sale</span>';
                }
                $htmlProducts .= '<div class="image">';
                $htmlProducts .= '<a class="lt-image" data-id="198" data-name="' . $data->title . '" data-title="' . $data->title . '" alt="' . $data->title . '" href="' . route('postProduct', [$data->id, $data->alias]) . '" >';
                $htmlProducts .= '<img  src="' . asset('upload/thumbnail/' . $data->images) . '" alt="' . $data->title . '"  class="img-1 product-featured-image">';
                $htmlProducts .= '<img  src="' . asset('upload/thumbnail/' . $data->images) . '" alt="' . $data->title . '"  class="img-2 product-featured-image">';
                $htmlProducts .= '</a>';
                $htmlProducts .= '<div class="button-group">';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '<div class="caption">';
                $htmlProducts .= '<h4>';
                $htmlProducts .= '<a data-id="198" data-name="' . $data->title . '" data-title="' . $data->title . '" alt="' . $data->title . '" href="' . route('postProduct', [$data->id, $data->alias]) . '" >';
                $htmlProducts .= $data->title;
                $htmlProducts .= '</a>';
                $htmlProducts .= '</h4>';
                $htmlProducts .= '<p class="price">';
                $htmlProducts .= '<span class="price-old">' . number_format($data->prices) . '₫</span>';
                $htmlProducts .= '<span class="price-new" style="margin-left: 5px">' . number_format($data->discount) . '₫</span>';
                $htmlProducts .= '</p>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
            }
        }
        //TODO breadcrumb
        $getCateName = DB::table('cate_prods')
            ->select('name','id','alias')
            ->where('id',$id)
            ->first();
        $htmlBreadcrumb .= '<div class="breadcrumbs">';
        $htmlBreadcrumb .= '<ul class="breadcrumb">';
        $htmlBreadcrumb .= '<li><a href="' . route('homePage') . '" data-name="trang-chu" data-title="Trang chủ"><i class="fa fa-home"></i> Trang chủ</a><span class="divider"></span></li>';
        $htmlBreadcrumb .= '<li><a href="' . route('getListProduct') . '" data-name="du-an" data-title="Sản phẩm">Sản phẩm</a><span class="divider"></span></li>';
        $htmlBreadcrumb .= '<li>' . $getCateName->name. '</li>';
        $htmlBreadcrumb .= '</ul>';
        $htmlBreadcrumb .= '</div>';
        return view('flexshop.product')->with(['thisCateProduct' => $htmlCate, 'thisProductList' => $htmlProducts,'thisBreadcrumb'=>$htmlBreadcrumb]);
    }

    public function listProduct($id)
    {
        $htmlProducts = '';
        $htmlCate = '';
        $htmlBreadcrumb= '';
        //TODO get Category
        $getCateId = DB::table('child_prods')->where('cateParen_id', $id)->first()->lvl;
        $getCate = DB::table('cate_prods as cate')
            ->rightjoin('child_prods as chil', 'cate.id', '=', 'chil.cateParen_id')
            ->select('cate.id as idCate', 'cate.name', 'cate.alias', 'cate.weight', 'chil.lvl')
            ->where('chil.lvl', $getCateId)
            ->get();
        foreach ($getCate as $cate) {
            $htmlCate .= '<li class="menu_lv1 item-sub-cat"><a data-name="san-pham" data-title="' . $cate->name . '" href="' . route('listProduct', [$cate->idCate, $cate->alias]) . '" ><i class="fa fa-angle-double-right" aria-hidden="true"></i> ' . $cate->name . '</a></li>';
        }
        //TODO get Products
        $getData = DB::table('products')
            ->select('id', 'title', 'alias', 'images', 'prices', 'discount', 'hot', 'active')
            ->where([
                ['active', 1],
                ['hot', 1],
                ['Cate_id', $id]
            ])
            ->orderBy('id', 'DESC')
            ->take(12)
            ->get();
        foreach ($getData as $data) {
            if (!empty($data)) {
                $htmlProducts .= '<div class="col-xs-12 col-sm-4 col-md-3">';
                $htmlProducts .= '<div  id="product-1003351994" class="item product-inner">';
                $htmlProducts .= '<div class="item-inner transition">';
                if (isset($data->hot)) {
                    $htmlProducts .= '<span class="label label-sale">Sale</span>';
                }
                $htmlProducts .= '<div class="image">';
                $htmlProducts .= '<a class="lt-image" data-id="198" data-name="' . $data->title . '" data-title="' . $data->title . '" alt="' . $data->title . '" href="' . route('postProduct', [$data->id, $data->alias]) . '" >';
                $htmlProducts .= '<img  src="' . asset('upload/thumbnail/' . $data->images) . '" alt="' . $data->title . '"  class="img-1 product-featured-image">';
                $htmlProducts .= '<img  src="' . asset('upload/thumbnail/' . $data->images) . '" alt="' . $data->title . '"  class="img-2 product-featured-image">';
                $htmlProducts .= '</a>';
                $htmlProducts .= '<div class="button-group">';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '<div class="caption">';
                $htmlProducts .= '<h4>';
                $htmlProducts .= '<a data-id="198" data-name="' . $data->title . '" data-title="' . $data->title . '" alt="' . $data->title . '" href="' . route('postProduct', [$data->id, $data->alias]) . '" >';
                $htmlProducts .= $data->title;
                $htmlProducts .= '</a>';
                $htmlProducts .= '</h4>';
                $htmlProducts .= '<p class="price">';
                $htmlProducts .= '<span class="price-old">' . number_format($data->prices) . '₫</span>';
                $htmlProducts .= '<span class="price-new" style="margin-left: 5px">' . number_format($data->discount) . '₫</span>';
                $htmlProducts .= '</p>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
                $htmlProducts .= '</div>';
            }
        }
        //TODO breadcrumb
        $getCateName= $getCate->first()->name;
        $getIdAlias= DB::table('cate_prods')
            ->select('id','alias','name')
            ->where('id',$getCateId)
            ->first();
        $htmlBreadcrumb .= '<div class="breadcrumbs">';
        $htmlBreadcrumb .= '<ul class="breadcrumb">';
        $htmlBreadcrumb .= '<li><a href="' . route('homePage') . '" data-name="trang-chu" data-title="Trang chủ"><i class="fa fa-home"></i> Trang chủ</a><span class="divider"></span></li>';
        $htmlBreadcrumb .= '<li><a href="' . route('getListProduct') . '" data-name="du-an" data-title="Sản phẩm">Sản phẩm</a><span class="divider"></span></li>';
        $htmlBreadcrumb .= '<li><a href="' . route('cateListProduct',[$getIdAlias->id,$getIdAlias->alias]) . '" data-name="du-an" data-title="Sản phẩm">'.$getIdAlias->name.'</a><span class="divider"></span></li>';
        $htmlBreadcrumb .= '<li>' . $getCateName. '</li>';
        $htmlBreadcrumb .= '</ul>';
        $htmlBreadcrumb .= '</div>';

        return view('flexshop.product')->with(['thisCateProduct' => $htmlCate, 'thisProductList' => $htmlProducts,'thisBreadcrumb'=>$htmlBreadcrumb]);
    }

    public function singleProduct($id)
    {
        //TODO get product
        $htmlRelate = '';
        $getData = DB::table('products')
            ->select('Cate_id', 'title', 'metaTitle', 'alias', 'summary', 'description', 'content', 'images', 'prices', 'discount')
            ->where('id', $id)
            ->first();
        //TODO relate products
        $getRelate = DB::table('products as n')
            ->select('n.id', 'n.title', 'n.alias', 'n.images', 'n.prices', 'n.discount', 'n.active')
            ->where([
                ['n.Cate_id', $getData->Cate_id],
                ['n.id', '<>', $id],
                ['n.active', 1]
            ])
            ->orderBy('n.id', 'DESC')
            ->take(5)
            ->get();
        if (isset($getRelate)) {
            foreach ($getRelate as $relate) {
                $htmlRelate .= '<div id="product-1003351965" class="item product-inner">';
                $htmlRelate .= '<div class="item-inner transition">';
                $htmlRelate .= '<div class="image">';
                $htmlRelate .= '<a class="lt-image" data-name="san-pham" data-title="' . $relate->title . '" alt="' . $relate->title . '" href="' . route('postProduct', [$relate->id, $relate->alias]) . '">';
                $htmlRelate .= '<img src="' . asset('upload/thumbnail/' . $relate->images) . '" class="img-1 product-featured-image" alt="' . $relate->title . '">';
                $htmlRelate .= '</a>';
                $htmlRelate .= '</div>';
                $htmlRelate .= '<div class="caption">';
                $htmlRelate .= '<h4>';
                $htmlRelate .= '<a data-name="san-pham" data-title="' . $relate->title . '" alt="' . $relate->title . '" href="' . route('postProduct', [$relate->id, $relate->alias]) . '">';
                $htmlRelate .= $relate->title;
                $htmlRelate .= '</a>';
                $htmlRelate .= '</h4>';
                $htmlRelate .= '<p class="price">';
                if ($relate->discount != 0) {
                    $htmlRelate .= '<span class="price-old">' . number_format($relate->prices) . 'đ</span>';
                    $htmlRelate .= '<span class="price-new">  ' . number_format($relate->discount) . '₫</span>';
                } elseif ($relate->prices == 0) {
                    $htmlRelate .= '<span class="price-new">Liên hệ</span>';
                } elseif ($relate->prices != 0) {
                    $htmlRelate .= '<span class="price-new">' . number_format($relate->prices) . 'đ</span>';
                } elseif ($relate->prices == 0 && $relate->discount != 0) {
                    $htmlRelate .= '<span class="price-new">Liên hệ</span>';
                } else {
                    $htmlRelate .= '<span class="price-old">' . number_format($relate->prices) . 'đ</span>';
                }
                $htmlRelate .= '</p>';
                $htmlRelate .= '</div>';
                $htmlRelate .= '</div>';
                $htmlRelate .= '</div>';
            }
        }
        return view('flexshop.singleProduct')->with(['data' => $getData, 'thisRelate' => $htmlRelate]);
    }

}
