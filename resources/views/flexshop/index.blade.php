@extends('flexshop')
@section('meta')
    <base href="http://phuc.viettechcorp.vn/webmau/flexshop/"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('upload/logo.png')}}"/>
    <title>Flexshop | Trang chủ</title>
    <script type="text/javascript" src="{{asset('admin/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <link
            rel="stylesheet"
            type="text/css"
            href="{{asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}"/>
    <script type="text/javascript" src="{{asset('views/template/src/bootstrap.minb09c.js')}}"></script>
    <link
            href="{{asset('views/template/src/bootstrap.minb09cb09c.css?v=180')}}"
            rel='stylesheet'
            type='text/css'
            media='all'/>
    <script
            defer="defer"
            type="text/javascript"
            src="{{asset('admin/plugins/nprogress/nprogress.js')}}"></script>
    <link
            rel="stylesheet"
            type="text/css"
            href="{{asset('admin/plugins/nprogress/nprogress.css')}}"/>
    <link
            rel="stylesheet"
            type="text/css"
            href="{{asset('admin/plugins/bootstrap-dropdown/css/animate.min.css')}}"/>
    <link
            rel="stylesheet"
            type="text/css"
            href="{{asset('admin/plugins/bootstrap-dropdown/css/bootstrap-dropdownhover.min.css')}}"/>
    <script
            defer="defer"
            type="text/javascript"
            src="{{asset('admin/plugins/bootstrap-dropdown/js/bootstrap-dropdownhover.min.js')}}"></script>
    <script defer="defer" type="text/javascript" src="{{asset('admin/plugins/wow/wow.min.js')}}"></script>
    <script defer="defer" type="text/javascript" src="{{asset('admin/assets/js/custom.js')}}"></script>
    <link
            href="{{asset('views/template/src/filecssb09cb09c.css?v=180')}}"
            rel='stylesheet'
            type='text/css'
            media='all'/>
    <link
            href="{{asset('views/template/src/owl.carouselb09cb09c.css?v=180')}}"
            rel='stylesheet'
            type='text/css'
            media='all'/>
    <link
            href="{{asset('views/template/src/dq.scssb09cb09c.css?v=180')}}"
            rel='stylesheet'
            type='text/css'
            media='all'/>
    <link
            href="{{asset('views/template/src/font-awesomeb09cb09c.css?v=180')}}"
            rel='stylesheet'
            type='text/css'
            media='all'/>
    <link
            href="{{asset('views/template/src/jquery.fancyboxb09cb09c.css?v=180')}}"
            rel='stylesheet'
            type='text/css'
            media='all'/>
    <link
            href="{{asset('views/template/src/styleb09cb09c.css?v=180')}}"
            rel='stylesheet'
            type='text/css'
            media='all'/>
    <link
            href='https://fonts.googleapis.com/css?family=Poppins:400,700,500,600'
            rel='stylesheet'
            type='text/css'>
    <div id="fb-root"></div>
    <address class="vcard" style="display:none">
        <img src='{{asset('upload/logo.png')}}' title='Flexshop' alt='Flexshop' class="photo"/>
        <a class="url fn" href="index.html">Flexshop</a>
        <div class="org">Flexshop Co, Ltd</div>
        <div class="adr">
            <div class="street-address">68/1D Chu Văn An. phường 26, quận Bình Thạnh</div>
            <span class="locality">Ho Chi Minh</span>,
            <span class="region">Binh Thanh</span>
            <span class="postal-code">70000</span>
        </div>
        <div class="tel">0934 154 886</div>
    </address>
    <div itemtype="http://schema.org/website ">
        <div itemtype="http://schema.org/website" itemscope="">
            <div>
                        <span itemprop="keywords">
                            <a rel="tag" href="index.html"></a>
                        </span>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="contentAjax">
        <div hidden="hidden">
            <h1>Flexshop</h1>
            <h2>Với đội ngũ nhân sự giàu kinh nghiệm, chúng tôi tự tin khẳng định mình có
                thể cung cấp dịch vụ tốt nhất và uy tín nhất.</h2>
            <h3>Flexshop</h3>
        </div>
        <div class="so-slideshow">
            <div class="module sohomepage-slider ">
                <div class="modcontent">
                    <div id="sohomepage-slider1">
                        <div class="loader"></div>
                        <div class="so-homeslider sohomeslider-inner-1">
                            <div class="item ">
                                <a href="#" title="Flexshop">
                                    <img class="responsive" src="upload/slide3.jpg" alt="Flexshop"/>
                                </a>
                            </div>
                            <div class="item ">
                                <a href="#" title="Flexshop">
                                    <img class="responsive" src="upload/slide2.jpg" alt="Flexshop"/>
                                </a>
                            </div>
                            <div class="item ">
                                <a href="#" title="Flexshop">
                                    <img class="responsive" src="upload/slide1.jpg" alt="Flexshop"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.modcontent-->
            </div>
            <!--/.module-->
            <div class="clearfix"></div>
        </div>
        <section class="so-spotlight1">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="module moduleship">
                            <div class="modcontent clearfix">
                                <div class="row re-ship-phone">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="item">
                                            <span class="icon icon1"></span>
                                            <p class="des">
                                                <span>Tư vấn 24/7</span>
                                                Miễn phí
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="item">
                                            <span class="icon icon2"></span>
                                            <p class="des">
                                                Vận chuyển
                                                <span>miễn phí</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="item">
                                            <span class="icon icon3"></span>
                                            <p class="des">
                                                Nhận hàng
                                                <span>Nhận tiền</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="item">
                                            <span class="icon icon4"></span>
                                            <p class="des">
                                                Gọi ngay
                                                <span>0934 154 886</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="box-collection2 highLightProject">
            <div class="container">
                <div class="modcontent">
                    <div class="header-title">
                        <h3 class="modtitle">
                            <span>Dự án nổi bật</span>
                        </h3>
                    </div>
                    <div class="product-slider-2  product-thumb">
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="170"
                                            data-name="san-pham"
                                            data-title="Ghế gỗ"
                                            alt="Ghế gỗ"
                                            href="partial/ghe-go--170.html">
                                        <img
                                                src="{{asset('upload/woodenfurnituresale-06-01-2017-11-08-09.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/woodenfurnituresale-06-01-2017-11-08-09.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="170"
                                                    data-name="san-pham"
                                                    data-title="Ghế gỗ"
                                                    alt="Ghế gỗ"
                                                    href="partial/ghe-go--170.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="170"
                                                data-name="san-pham"
                                                data-title="Ghế gỗ"
                                                alt="Ghế gỗ"
                                                href="partial/ghe-go--170.html">
                                            Ghế gỗ
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="168"
                                            data-name="san-pham"
                                            data-title="Sofa ghế băng"
                                            alt="Sofa ghế băng"
                                            href="san-pham/sofa-ghe-bang-168.html">
                                        <img
                                                src="{{asset('upload/gy-06-01-2017-11-06-49.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/gy-06-01-2017-11-06-49.html')}}"
                                                class="img-2     product-featured-image"
                                                alt="">
                                    </a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="168"
                                                    data-name="san-pham"
                                                    data-title="Sofa ghế băng"
                                                    alt="Sofa ghế băng"
                                                    href="san-pham/sofa-ghe-bang-168.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="168"
                                                data-name="san-pham"
                                                data-title="Sofa ghế băng"
                                                alt="Sofa ghế băng"
                                                href="san-pham/sofa-ghe-bang-168.html">
                                            Sofa ghế băng
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="167"
                                            data-name="san-pham"
                                            data-title="Sofa cổ điển, cao cấp"
                                            alt="Sofa cổ điển, cao cấp"
                                            href="san-pham/sofa-co-die-n-cao-ca-p-167.html">
                                        <img
                                                src="{{asset('upload/410974-06-01-2017-11-06-21.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/410974-06-01-2017-11-06-21.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="167"
                                                    data-name="san-pham"
                                                    data-title="Sofa cổ điển, cao cấp"
                                                    alt="Sofa cổ điển, cao cấp"
                                                    href="san-pham/sofa-co-die-n-cao-ca-p-167.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="167"
                                                data-name="san-pham"
                                                data-title="Sofa cổ điển, cao cấp"
                                                alt="Sofa cổ điển, cao cấp"
                                                href="san-pham/sofa-co-die-n-cao-ca-p-167.html">
                                            Sofa cổ điển, cao cấp
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="166"
                                            data-name="san-pham"
                                            data-title="Tủ đề giày"
                                            alt="Tủ đề giày"
                                            href="san-pham/tu-de-gia-y-166.html">
                                        <img
                                                src="{{asset('upload/rciwxav47pdp1shoerackwithcherr-06-01-2017-11-05-29.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/rciwxav47pdp1shoerackwithcherr-06-01-2017-11-05-29.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="166"
                                                    data-name="san-pham"
                                                    data-title="Tủ đề giày"
                                                    alt="Tủ đề giày"
                                                    href="san-pham/tu-de-gia-y-166.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="166"
                                                data-name="san-pham"
                                                data-title="Tủ đề giày"
                                                alt="Tủ đề giày"
                                                href="san-pham/tu-de-gia-y-166.html">
                                            Tủ đề giày
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="165"
                                            data-name="san-pham"
                                            data-title="Tủ đề giày gác chéo"
                                            alt="Tủ đề giày gác chéo"
                                            href="san-pham/tu-de-gia-y-ga-c-che-o-165.html">
                                        <img
                                                src="{{asset('upload/shoerackopen-06-01-2017-11-05-01.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/shoerackopen-06-01-2017-11-05-01.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="165"
                                                    data-name="san-pham"
                                                    data-title="Tủ đề giày gác chéo"
                                                    alt="Tủ đề giày gác chéo"
                                                    href="san-pham/tu-de-gia-y-ga-c-che-o-165.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="165"
                                                data-name="san-pham"
                                                data-title="Tủ đề giày gác chéo"
                                                alt="Tủ đề giày gác chéo"
                                                href="san-pham/tu-de-gia-y-ga-c-che-o-165.html">
                                            Tủ đề giày gác chéo
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="164"
                                            data-name="san-pham"
                                            data-title="Tủ rượu 3 ngăn"
                                            alt="Tủ rượu 3 ngăn"
                                            href="san-pham/tu-ruo-u-3-ngan-164.html">
                                        <img
                                                src="{{asset('upload/18-06-01-2017-11-04-25.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/18-06-01-2017-11-04-25.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="164"
                                                    data-name="san-pham"
                                                    data-title="Tủ rượu 3 ngăn"
                                                    alt="Tủ rượu 3 ngăn"
                                                    href="san-pham/tu-ruo-u-3-ngan-164.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="164"
                                                data-name="san-pham"
                                                data-title="Tủ rượu 3 ngăn"
                                                alt="Tủ rượu 3 ngăn"
                                                href="san-pham/tu-ruo-u-3-ngan-164.html">
                                            Tủ rượu 3 ngăn
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="163"
                                            data-name="san-pham"
                                            data-title="Bàn làm việc hiện đại"
                                            alt="Bàn làm việc hiện đại"
                                            href="san-pham/ba-n-la-m-vie-c-hie-n-da-i-163.html">
                                        <img
                                                src="{{asset('upload/edit-06-01-2017-11-03-38.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/edit-06-01-2017-11-03-38.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="163"
                                                    data-name="san-pham"
                                                    data-title="Bàn làm việc hiện đại"
                                                    alt="Bàn làm việc hiện đại"
                                                    href="san-pham/ba-n-la-m-vie-c-hie-n-da-i-163.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="163"
                                                data-name="san-pham"
                                                data-title="Bàn làm việc hiện đại"
                                                alt="Bàn làm việc hiện đại"
                                                href="san-pham/ba-n-la-m-vie-c-hie-n-da-i-163.html">
                                            Bàn làm việc hiện đại
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="162"
                                            data-name="san-pham"
                                            data-title="Bàn sofa 01"
                                            alt="Bàn sofa 01"
                                            href="san-pham/ba-n-sofa-01-162.html">
                                        <img
                                                src="{{asset('upload/5169-06-01-2017-11-02-52.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/5169-06-01-2017-11-02-52.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="162"
                                                    data-name="san-pham"
                                                    data-title="Bàn sofa 01"
                                                    alt="Bàn sofa 01"
                                                    href="san-pham/ba-n-sofa-01-162.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="162"
                                                data-name="san-pham"
                                                data-title="Bàn sofa 01"
                                                alt="Bàn sofa 01"
                                                href="san-pham/ba-n-sofa-01-162.html">
                                            Bàn sofa 01
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="161"
                                            data-name="san-pham"
                                            data-title="Bàn sofa hiện đại"
                                            alt="Bàn sofa hiện đại"
                                            href="san-pham/ba-n-sofa-hie-n-da-i-161.html">
                                        <img
                                                src="{{asset('upload/bansofa03-06-01-2017-11-01-46.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/bansofa03-06-01-2017-11-01-46.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="161"
                                                    data-name="san-pham"
                                                    data-title="Bàn sofa hiện đại"
                                                    alt="Bàn sofa hiện đại"
                                                    href="san-pham/ba-n-sofa-hie-n-da-i-161.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="161"
                                                data-name="san-pham"
                                                data-title="Bàn sofa hiện đại"
                                                alt="Bàn sofa hiện đại"
                                                href="san-pham/ba-n-sofa-hie-n-da-i-161.html">
                                            Bàn sofa hiện đại
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="160"
                                            data-name="san-pham"
                                            data-title="Sofa nỉ hồng"
                                            alt="Sofa nỉ hồng"
                                            href="san-pham/sofa-ni-ho-ng-160.html">
                                        <img
                                                src="{{asset('upload/sofa02-06-01-2017-11-00-56.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/sofa02-06-01-2017-11-00-56.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="160"
                                                    data-name="san-pham"
                                                    data-title="Sofa nỉ hồng"
                                                    alt="Sofa nỉ hồng"
                                                    href="san-pham/sofa-ni-ho-ng-160.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="160"
                                                data-name="san-pham"
                                                data-title="Sofa nỉ hồng"
                                                alt="Sofa nỉ hồng"
                                                href="san-pham/sofa-ni-ho-ng-160.html">
                                            Sofa nỉ hồng
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="159"
                                            data-name="san-pham"
                                            data-title="Ghế lười"
                                            alt="Ghế lười"
                                            href="san-pham/ghe-luo-i-159.html">
                                        <img
                                                src="{{asset('upload/gheluoisofa5-06-01-2017-11-00-03.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/gheluoisofa5-06-01-2017-11-00-03.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="159"
                                                    data-name="san-pham"
                                                    data-title="Ghế lười"
                                                    alt="Ghế lười"
                                                    href="san-pham/ghe-luo-i-159.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="159"
                                                data-name="san-pham"
                                                data-title="Ghế lười"
                                                alt="Ghế lười"
                                                href="san-pham/ghe-luo-i-159.html">
                                            Ghế lười
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="158"
                                            data-name="san-pham"
                                            data-title="Sofa đơn tím sang trọng"
                                            alt="Sofa đơn tím sang trọng"
                                            href="san-pham/sofa-don-ti-m-sang-tro-ng-158.html">
                                        <img
                                                src="{{asset('upload/steelcasecoalessehosusofabluep-06-01-2017-10-59-05.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/steelcasecoalessehosusofabluep-06-01-2017-10-59-05.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="158"
                                                    data-name="san-pham"
                                                    data-title="Sofa đơn tím sang trọng"
                                                    alt="Sofa đơn tím sang trọng"
                                                    href="san-pham/sofa-don-ti-m-sang-tro-ng-158.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="158"
                                                data-name="san-pham"
                                                data-title="Sofa đơn tím sang trọng"
                                                alt="Sofa đơn tím sang trọng"
                                                href="san-pham/sofa-don-ti-m-sang-tro-ng-158.html">
                                            Sofa đơn tím sang trọng
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Banner-->
        <section class="box-collection2">
            <div class="container">
                <div class="modcontent">
                    <div class="header-title">
                        <h3 class="modtitle">
                            <span>Dự án thiết kế</span>
                        </h3>
                    </div>
                    <div class="product-slider-2  product-thumb">
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="170"
                                            data-name="san-pham"
                                            data-title="Ghế gỗ"
                                            alt="Ghế gỗ"
                                            href="partial/ghe-go--170.html">
                                        <img
                                                src="{{asset('upload/woodenfurnituresale-06-01-2017-11-08-09.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/woodenfurnituresale-06-01-2017-11-08-09.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="170"
                                                    data-name="san-pham"
                                                    data-title="Ghế gỗ"
                                                    alt="Ghế gỗ"
                                                    href="partial/ghe-go--170.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="170"
                                                data-name="san-pham"
                                                data-title="Ghế gỗ"
                                                alt="Ghế gỗ"
                                                href="partial/ghe-go--170.html">
                                            Ghế gỗ
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="168"
                                            data-name="san-pham"
                                            data-title="Sofa ghế băng"
                                            alt="Sofa ghế băng"
                                            href="san-pham/sofa-ghe-bang-168.html">
                                        <img
                                                src="{{asset('upload/gy-06-01-2017-11-06-49.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/gy-06-01-2017-11-06-49.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="168"
                                                    data-name="san-pham"
                                                    data-title="Sofa ghế băng"
                                                    alt="Sofa ghế băng"
                                                    href="san-pham/sofa-ghe-bang-168.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="168"
                                                data-name="san-pham"
                                                data-title="Sofa ghế băng"
                                                alt="Sofa ghế băng"
                                                href="san-pham/sofa-ghe-bang-168.html">
                                            Sofa ghế băng
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="167"
                                            data-name="san-pham"
                                            data-title="Sofa cổ điển, cao cấp"
                                            alt="Sofa cổ điển, cao cấp"
                                            href="san-pham/sofa-co-die-n-cao-ca-p-167.html">
                                        <img
                                                src="{{asset('upload/410974-06-01-2017-11-06-21.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/410974-06-01-2017-11-06-21.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="167"
                                                    data-name="san-pham"
                                                    data-title="Sofa cổ điển, cao cấp"
                                                    alt="Sofa cổ điển, cao cấp"
                                                    href="san-pham/sofa-co-die-n-cao-ca-p-167.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="167"
                                                data-name="san-pham"
                                                data-title="Sofa cổ điển, cao cấp"
                                                alt="Sofa cổ điển, cao cấp"
                                                href="san-pham/sofa-co-die-n-cao-ca-p-167.html">
                                            Sofa cổ điển, cao cấp
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="166"
                                            data-name="san-pham"
                                            data-title="Tủ đề giày"
                                            alt="Tủ đề giày"
                                            href="san-pham/tu-de-gia-y-166.html">
                                        <img
                                                src="{{asset('upload/rciwxav47pdp1shoerackwithcherr-06-01-2017-11-05-29.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/rciwxav47pdp1shoerackwithcherr-06-01-2017-11-05-29.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="166"
                                                    data-name="san-pham"
                                                    data-title="Tủ đề giày"
                                                    alt="Tủ đề giày"
                                                    href="san-pham/tu-de-gia-y-166.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="166"
                                                data-name="san-pham"
                                                data-title="Tủ đề giày"
                                                alt="Tủ đề giày"
                                                href="san-pham/tu-de-gia-y-166.html">
                                            Tủ đề giày
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="165"
                                            data-name="san-pham"
                                            data-title="Tủ đề giày gác chéo"
                                            alt="Tủ đề giày gác chéo"
                                            href="san-pham/tu-de-gia-y-ga-c-che-o-165.html">
                                        <img
                                                src="{{asset('upload/shoerackopen-06-01-2017-11-05-01.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/shoerackopen-06-01-2017-11-05-01.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="165"
                                                    data-name="san-pham"
                                                    data-title="Tủ đề giày gác chéo"
                                                    alt="Tủ đề giày gác chéo"
                                                    href="san-pham/tu-de-gia-y-ga-c-che-o-165.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="165"
                                                data-name="san-pham"
                                                data-title="Tủ đề giày gác chéo"
                                                alt="Tủ đề giày gác chéo"
                                                href="san-pham/tu-de-gia-y-ga-c-che-o-165.html">
                                            Tủ đề giày gác chéo
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="164"
                                            data-name="san-pham"
                                            data-title="Tủ rượu 3 ngăn"
                                            alt="Tủ rượu 3 ngăn"
                                            href="san-pham/tu-ruo-u-3-ngan-164.html">
                                        <img
                                                src="{{asset('upload/18-06-01-2017-11-04-25.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/18-06-01-2017-11-04-25.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="164"
                                                    data-name="san-pham"
                                                    data-title="Tủ rượu 3 ngăn"
                                                    alt="Tủ rượu 3 ngăn"
                                                    href="san-pham/tu-ruo-u-3-ngan-164.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="164"
                                                data-name="san-pham"
                                                data-title="Tủ rượu 3 ngăn"
                                                alt="Tủ rượu 3 ngăn"
                                                href="san-pham/tu-ruo-u-3-ngan-164.html">
                                            Tủ rượu 3 ngăn
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="163"
                                            data-name="san-pham"
                                            data-title="Bàn làm việc hiện đại"
                                            alt="Bàn làm việc hiện đại"
                                            href="san-pham/ba-n-la-m-vie-c-hie-n-da-i-163.html">
                                        <img
                                                src="{{asset('upload/edit-06-01-2017-11-03-38.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/edit-06-01-2017-11-03-38.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="163"
                                                    data-name="san-pham"
                                                    data-title="Bàn làm việc hiện đại"
                                                    alt="Bàn làm việc hiện đại"
                                                    href="san-pham/ba-n-la-m-vie-c-hie-n-da-i-163.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="163"
                                                data-name="san-pham"
                                                data-title="Bàn làm việc hiện đại"
                                                alt="Bàn làm việc hiện đại"
                                                href="san-pham/ba-n-la-m-vie-c-hie-n-da-i-163.html">
                                            Bàn làm việc hiện đại
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="162"
                                            data-name="san-pham"
                                            data-title="Bàn sofa 01"
                                            alt="Bàn sofa 01"
                                            href="san-pham/ba-n-sofa-01-162.html">
                                        <img
                                                src="{{asset('upload/5169-06-01-2017-11-02-52.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/5169-06-01-2017-11-02-52.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="162"
                                                    data-name="san-pham"
                                                    data-title="Bàn sofa 01"
                                                    alt="Bàn sofa 01"
                                                    href="san-pham/ba-n-sofa-01-162.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="162"
                                                data-name="san-pham"
                                                data-title="Bàn sofa 01"
                                                alt="Bàn sofa 01"
                                                href="san-pham/ba-n-sofa-01-162.html">
                                            Bàn sofa 01
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="161"
                                            data-name="san-pham"
                                            data-title="Bàn sofa hiện đại"
                                            alt="Bàn sofa hiện đại"
                                            href="san-pham/ba-n-sofa-hie-n-da-i-161.html">
                                        <img
                                                src="{{asset('upload/bansofa03-06-01-2017-11-01-46.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/bansofa03-06-01-2017-11-01-46.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="161"
                                                    data-name="san-pham"
                                                    data-title="Bàn sofa hiện đại"
                                                    alt="Bàn sofa hiện đại"
                                                    href="san-pham/ba-n-sofa-hie-n-da-i-161.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="161"
                                                data-name="san-pham"
                                                data-title="Bàn sofa hiện đại"
                                                alt="Bàn sofa hiện đại"
                                                href="san-pham/ba-n-sofa-hie-n-da-i-161.html">
                                            Bàn sofa hiện đại
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="160"
                                            data-name="san-pham"
                                            data-title="Sofa nỉ hồng"
                                            alt="Sofa nỉ hồng"
                                            href="san-pham/sofa-ni-ho-ng-160.html">
                                        <img
                                                src="{{asset('upload/sofa02-06-01-2017-11-00-56.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/sofa02-06-01-2017-11-00-56.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="160"
                                                    data-name="san-pham"
                                                    data-title="Sofa nỉ hồng"
                                                    alt="Sofa nỉ hồng"
                                                    href="san-pham/sofa-ni-ho-ng-160.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="160"
                                                data-name="san-pham"
                                                data-title="Sofa nỉ hồng"
                                                alt="Sofa nỉ hồng"
                                                href="san-pham/sofa-ni-ho-ng-160.html">
                                            Sofa nỉ hồng
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="159"
                                            data-name="san-pham"
                                            data-title="Ghế lười"
                                            alt="Ghế lười"
                                            href="san-pham/ghe-luo-i-159.html">
                                        <img
                                                src="{{asset('upload/gheluoisofa5-06-01-2017-11-00-03.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/gheluoisofa5-06-01-2017-11-00-03.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="159"
                                                    data-name="san-pham"
                                                    data-title="Ghế lười"
                                                    alt="Ghế lười"
                                                    href="san-pham/ghe-luo-i-159.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="159"
                                                data-name="san-pham"
                                                data-title="Ghế lười"
                                                alt="Ghế lười"
                                                href="san-pham/ghe-luo-i-159.html">
                                            Ghế lười
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="158"
                                            data-name="san-pham"
                                            data-title="Sofa đơn tím sang trọng"
                                            alt="Sofa đơn tím sang trọng"
                                            href="san-pham/sofa-don-ti-m-sang-tro-ng-158.html">
                                        <img
                                                src="{{asset('upload/steelcasecoalessehosusofabluep-06-01-2017-10-59-05.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/steelcasecoalessehosusofabluep-06-01-2017-10-59-05.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="158"
                                                    data-name="san-pham"
                                                    data-title="Sofa đơn tím sang trọng"
                                                    alt="Sofa đơn tím sang trọng"
                                                    href="san-pham/sofa-don-ti-m-sang-tro-ng-158.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="158"
                                                data-name="san-pham"
                                                data-title="Sofa đơn tím sang trọng"
                                                alt="Sofa đơn tím sang trọng"
                                                href="san-pham/sofa-don-ti-m-sang-tro-ng-158.html">
                                            Sofa đơn tím sang trọng
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="box-collection-hai">
            <div class="container">
                <div class="modcontent">
                    <div class="header-title">
                        <h3 class="modtitle">
                            <span>Dự án thi công nội thất</span>
                        </h3>
                    </div>
                    <div class="product-slider-2  product-thumb">
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="196"
                                            data-name="san-pham"
                                            data-title="Vòi lavabo"
                                            alt="Vòi lavabo"
                                            href="san-pham/vo-i-lavabo-196.html">
                                        <img
                                                src="upload/voi-lavabo-a908go-copy-300x300-06-01-2017-12-11-20.png"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="upload/voi-lavabo-a908go-copy-300x300-06-01-2017-12-11-20.png"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="196"
                                                    data-name="san-pham"
                                                    data-title="Vòi lavabo"
                                                    alt="Vòi lavabo"
                                                    href="san-pham/vo-i-lavabo-196.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="196"
                                                data-name="san-pham"
                                                data-title="Vòi lavabo"
                                                alt="Vòi lavabo"
                                                href="san-pham/vo-i-lavabo-196.html">
                                            Vòi lavabo
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="195"
                                            data-name="san-pham"
                                            data-title="Tay sen tròn Johnson Suisse Caspian II U00723"
                                            alt="Tay sen tròn Johnson Suisse Caspian II U00723"
                                            href="san-pham/tay-sen-tron-johnson-suisse-caspian-ii-u00723-195.html">
                                        <img
                                                src="upload/tay-sen-johnson-suisse-a00723-1-06-01-2017-12-11-44.jpg"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="upload/tay-sen-johnson-suisse-a00723-1-06-01-2017-12-11-44.jpg"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="195"
                                                    data-name="san-pham"
                                                    data-title="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                    alt="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                    href="san-pham/tay-sen-tron-johnson-suisse-caspian-ii-u00723-195.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="195"
                                                data-name="san-pham"
                                                data-title="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                alt="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                href="san-pham/tay-sen-tron-johnson-suisse-caspian-ii-u00723-195.html">
                                            Tay sen tròn Johnson Suisse Caspian II U00723
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="194"
                                            data-name="san-pham"
                                            data-title="Tay sen Johnson Suisse Arctic II U00720"
                                            alt="Tay sen Johnson Suisse Arctic II U00720"
                                            href="san-pham/tay-sen-johnson-suisse-arctic-ii-u00720-194.html">
                                        <img
                                                src="upload/tay-sen-johnson-suisse-a00720-2-06-01-2017-12-12-03.jpg"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="upload/tay-sen-johnson-suisse-a00720-2-06-01-2017-12-12-03.jpg"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="194"
                                                    data-name="san-pham"
                                                    data-title="Tay sen Johnson Suisse Arctic II U00720"
                                                    alt="Tay sen Johnson Suisse Arctic II U00720"
                                                    href="san-pham/tay-sen-johnson-suisse-arctic-ii-u00720-194.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="194"
                                                data-name="san-pham"
                                                data-title="Tay sen Johnson Suisse Arctic II U00720"
                                                alt="Tay sen Johnson Suisse Arctic II U00720"
                                                href="san-pham/tay-sen-johnson-suisse-arctic-ii-u00720-194.html">
                                            Tay sen Johnson Suisse Arctic II U00720
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <span class="label label-sale">Sale</span>
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="193"
                                            data-name="san-pham"
                                            data-title="Tay sen tròn Johnson Suisse Bering II U01072"
                                            alt="Tay sen tròn Johnson Suisse Bering II U01072"
                                            href="san-pham/tay-sen-tron-johnson-suisse-bering-ii-u01072-193.html">
                                        <img
                                                src="upload/tay-sen-a01072-2-600x600-06-01-2017-12-12-32.jpg"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="upload/tay-sen-a01072-2-600x600-06-01-2017-12-12-32.jpg"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="193"
                                                    data-name="san-pham"
                                                    data-title="Tay sen tròn Johnson Suisse Bering II U01072"
                                                    alt="Tay sen tròn Johnson Suisse Bering II U01072"
                                                    href="san-pham/tay-sen-tron-johnson-suisse-bering-ii-u01072-193.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="193"
                                                data-name="san-pham"
                                                data-title="Tay sen tròn Johnson Suisse Bering II U01072"
                                                alt="Tay sen tròn Johnson Suisse Bering II U01072"
                                                href="san-pham/tay-sen-tron-johnson-suisse-bering-ii-u01072-193.html">
                                            Tay sen tròn Johnson Suisse Bering II U01072
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="192"
                                            data-name="san-pham"
                                            data-title="Vòi sen mạ chrome Johnson Suisse Iris U00923"
                                            alt="Vòi sen mạ chrome Johnson Suisse Iris U00923"
                                            href="san-pham/voi-sen-ma-chrome-johnson-suisse-iris-u00923-192.html">
                                        <img
                                                src="upload/khoa-sen-iris-a00923-3-06-01-2017-12-12-46.jpg"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="upload/khoa-sen-iris-a00923-3-06-01-2017-12-12-46.jpg"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="192"
                                                    data-name="san-pham"
                                                    data-title="Vòi sen mạ chrome Johnson Suisse Iris U00923"
                                                    alt="Vòi sen mạ chrome Johnson Suisse Iris U00923"
                                                    href="san-pham/voi-sen-ma-chrome-johnson-suisse-iris-u00923-192.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="192"
                                                data-name="san-pham"
                                                data-title="Vòi sen mạ chrome Johnson Suisse Iris U00923"
                                                alt="Vòi sen mạ chrome Johnson Suisse Iris U00923"
                                                href="san-pham/voi-sen-ma-chrome-johnson-suisse-iris-u00923-192.html">
                                            Vòi sen mạ chrome Johnson Suisse Iris U00923
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="191"
                                            data-name="san-pham"
                                            data-title="Vòi sen Johnson Suisse Iris U00922"
                                            alt="Vòi sen Johnson Suisse Iris U00922"
                                            href="san-pham/voi-sen-johnson-suisse-iris-u00922-191.html">
                                        <img
                                                src="upload/khoa-sen-iris-a00922-2-06-01-2017-12-13-12.jpg"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="upload/khoa-sen-iris-a00922-2-06-01-2017-12-13-12.jpg"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="191"
                                                    data-name="san-pham"
                                                    data-title="Vòi sen Johnson Suisse Iris U00922"
                                                    alt="Vòi sen Johnson Suisse Iris U00922"
                                                    href="san-pham/voi-sen-johnson-suisse-iris-u00922-191.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="191"
                                                data-name="san-pham"
                                                data-title="Vòi sen Johnson Suisse Iris U00922"
                                                alt="Vòi sen Johnson Suisse Iris U00922"
                                                href="san-pham/voi-sen-johnson-suisse-iris-u00922-191.html">
                                            Vòi sen Johnson Suisse Iris U00922
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="190"
                                            data-name="san-pham"
                                            data-title="Máy nước nóng PANASONIC"
                                            alt="Máy nước nóng PANASONIC"
                                            href="san-pham/ma-y-nuo-c-no-ng-panasonic-190.html">
                                        <img
                                                src="{{asset('upload/nl03-06-01-2017-11-29-03.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/nl03-06-01-2017-11-29-03.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="190"
                                                    data-name="san-pham"
                                                    data-title="Máy nước nóng PANASONIC"
                                                    alt="Máy nước nóng PANASONIC"
                                                    href="san-pham/ma-y-nuo-c-no-ng-panasonic-190.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="190"
                                                data-name="san-pham"
                                                data-title="Máy nước nóng PANASONIC"
                                                alt="Máy nước nóng PANASONIC"
                                                href="san-pham/ma-y-nuo-c-no-ng-panasonic-190.html">
                                            Máy nước nóng PANASONIC
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item product-inner">
                            <div class="item-inner transition">
                                <div class="image">
                                    <a
                                            class="lt-image"
                                            data-id="189"
                                            data-name="san-pham"
                                            data-title="Thiết bị bật đèn thông minh"
                                            alt="Thiết bị bật đèn thông minh"
                                            href="san-pham/thie-t-bi-ba-t-de-n-thong-minh-189.html">
                                        <img
                                                src="{{asset('upload/thongminh-06-01-2017-11-29-37.html')}}"
                                                class="img-1 product-featured-image"
                                                alt="">
                                        <img
                                                src="{{asset('upload/thongminh-06-01-2017-11-29-37.html')}}"
                                                class="img-2     product-featured-image"
                                                alt=""></a>
                                    <div class="button-group">
                                        <div class="button-group">
                                            <a
                                                    data-id="189"
                                                    data-name="san-pham"
                                                    data-title="Thiết bị bật đèn thông minh"
                                                    alt="Thiết bị bật đèn thông minh"
                                                    href="san-pham/thie-t-bi-ba-t-de-n-thong-minh-189.html"
                                                    class="awe-button product-quick-view btn-quickview"
                                                    data-toggle="modal"
                                                    title="Xem chi tiết">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <!-- /.button-group -->
                                    </div>
                                    <!-- /.button-group -->
                                </div>
                                <!-- /.image -->
                                <div class="caption">
                                    <h4>
                                        <a
                                                data-id="189"
                                                data-name="san-pham"
                                                data-title="Thiết bị bật đèn thông minh"
                                                alt="Thiết bị bật đèn thông minh"
                                                href="san-pham/thie-t-bi-ba-t-de-n-thong-minh-189.html">
                                            Thiết bị bật đèn thông minh
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="box-collection1">
            <div class="container">
                <div class=" modcontent">
                    <div class="header-title">
                        <h3 class="modtitle">
                                    <span>Danh mục
                                    </span>&nbsp;<span>sản phẩm</span>
                        </h3>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3 hidden-xs">
                            <div class="box-colection">
                                <ul class="list-collections list-cate-banner">
                                    <li class="menu_lv1 item-sub-cat">
                                        <a
                                                data-name="san-pham"
                                                data-idList="939"
                                                data-title="Phòng khách"
                                                href="san-pham/danh-muc/phong-khach-939.html">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            Phòng khách
                                        </a>
                                    </li>
                                    <li class="menu_lv1 item-sub-cat">
                                        <a
                                                data-name="san-pham"
                                                data-idList="940"
                                                data-title="Phòng ngủ"
                                                href="san-pham/danh-muc/phong-ngu-940.html">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            Phòng ngủ
                                        </a>
                                    </li>
                                    <li class="menu_lv1 item-sub-cat">
                                        <a
                                                data-name="san-pham"
                                                data-idList="941"
                                                data-title="Nhà bếp"
                                                href="san-pham/danh-muc/nha-bep-941.html">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            Nhà bếp
                                        </a>
                                    </li>
                                    <li class="menu_lv1 item-sub-cat">
                                        <a
                                                data-name="san-pham"
                                                data-idList="942"
                                                data-title="Thiết bị vệ sinh"
                                                href="san-pham/danh-muc/thiet-bi-ve-sinh-942.html">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            Thiết bị vệ sinh
                                        </a>
                                    </li>
                                    <li class="menu_lv1 item-sub-cat">
                                        <a
                                                data-name="san-pham"
                                                data-idList="943"
                                                data-title="Đồ trang trí"
                                                href="san-pham/danh-muc/do-trang-tri-943.html">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            Đồ trang trí
                                        </a>
                                    </li>
                                    <li class="menu_lv1 item-sub-cat">
                                        <a
                                                data-name="san-pham"
                                                data-idList="944"
                                                data-title="Nhà thông minh"
                                                href="san-pham/danh-muc/nha-thong-minh-944.html">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            Nhà thông minh
                                        </a>
                                    </li>
                                    <li class="menu_lv1 item-sub-cat">
                                        <a
                                                data-name="san-pham"
                                                data-idList="945"
                                                data-title="Sofa"
                                                href="san-pham/danh-muc/sofa-945.html">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            Sofa
                                        </a>
                                    </li>
                                    <li class="menu_lv1 item-sub-cat">
                                        <a
                                                data-name="san-pham"
                                                data-idList="946"
                                                data-title="Phòng tắm"
                                                href="san-pham/danh-muc/phong-tam-946.html">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                            Phòng tắm
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pre-text banner hidden-xs">
                                <a href="#"><img src="{{asset('views/template/src/c1b09cb09c.jpg?v=180')}}"></a>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-md-9 col-xs-12">
                            <div class="product-slider-1  product-thumb">
                                <div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="202"
                                                            data-name="san-pham"
                                                            data-title="Thảm lông trải sàn xám kích cỡ 2*3 ATH041"
                                                            alt="Thảm lông trải sàn xám kích cỡ 2*3 ATH041"
                                                            href="san-pham/tham-long-trai-san-xam-kich-co-2-3-ath041-202.html">
                                                        <img
                                                                src="upload/tham-trai-san-ath040-ath041-2-600x600-06-01-2017-12-14-36.png"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/tham-trai-san-ath040-ath041-2-600x600-06-01-2017-12-14-36.png"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(202,'[data-id=202] img')">
                                                                                <span>
                                                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                                </span>
                                                            </button>
                                                            <a
                                                                    data-id="202"
                                                                    data-name="san-pham"
                                                                    data-title="Thảm lông trải sàn xám kích cỡ 2*3 ATH041"
                                                                    alt="Thảm lông trải sàn xám kích cỡ 2*3 ATH041"
                                                                    href="san-pham/tham-long-trai-san-xam-kich-co-2-3-ath041-202.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="202"
                                                                data-name="san-pham"
                                                                data-title="Thảm lông trải sàn xám kích cỡ 2*3 ATH041"
                                                                alt="Thảm lông trải sàn xám kích cỡ 2*3 ATH041"
                                                                href="san-pham/tham-long-trai-san-xam-kich-co-2-3-ath041-202.html">
                                                            Thảm lông trải sàn xám kích cỡ 2*3 ATH041
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-new">10,000,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="201"
                                                            data-name="san-pham"
                                                            data-title="Thảm trải sàn caro nâu kích cỡ 1.6*2.3 ATH056"
                                                            alt="Thảm trải sàn caro nâu kích cỡ 1.6*2.3 ATH056"
                                                            href="san-pham/tham-trai-san-caro-nau-kich-co-1-6-2-3-ath056-201.html">
                                                        <img
                                                                src="upload/tham-trai-san-caro-ath056-ath057-2-06-01-2017-12-14-54.png"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/tham-trai-san-caro-ath056-ath057-2-06-01-2017-12-14-54.png"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <a
                                                                    class="btn-addToCart awe-button product-add-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(201,'[data-id=201] img')">
                                                                <i class="icon icon-shopping-bag"></i>
                                                            </a>
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(201,'[data-id=201] img')">
                                                                            <span>
                                                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                            </span>
                                                            </button>
                                                            <a
                                                                    data-id="201"
                                                                    data-name="san-pham"
                                                                    data-title="Thảm trải sàn caro nâu kích cỡ 1.6*2.3 ATH056"
                                                                    alt="Thảm trải sàn caro nâu kích cỡ 1.6*2.3 ATH056"
                                                                    href="san-pham/tham-trai-san-caro-nau-kich-co-1-6-2-3-ath056-201.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                        <!-- /.button-group -->
                                                    </div>
                                                    <!-- /.button-group -->
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="201"
                                                                data-name="san-pham"
                                                                data-title="Thảm trải sàn caro nâu kích cỡ 1.6*2.3 ATH056"
                                                                alt="Thảm trải sàn caro nâu kích cỡ 1.6*2.3 ATH056"
                                                                href="san-pham/tham-trai-san-caro-nau-kich-co-1-6-2-3-ath056-201.html">
                                                            Thảm trải sàn caro nâu kích cỡ 1.6*2.3 ATH056
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-new">13,000,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                </div>
                                <div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="200"
                                                            data-name="san-pham"
                                                            data-title="Đồng hồ trang trí ATT461"
                                                            alt="Đồng hồ trang trí ATT461"
                                                            href="san-pham/dong-ho-trang-tri-att461-200.html">
                                                        <img
                                                                src="upload/dong-ho-trang-tri-att461-600x600-06-01-2017-12-15-33.png"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/dong-ho-trang-tri-att461-600x600-06-01-2017-12-15-33.png"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <a
                                                                    class="btn-addToCart awe-button product-add-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(200,'[data-id=200] img')">
                                                                <i class="icon icon-shopping-bag"></i>
                                                            </a>
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(200,'[data-id=200] img')">
                                                                                <span>
                                                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                                </span>
                                                            </button>
                                                            <a
                                                                    data-id="200"
                                                                    data-name="san-pham"
                                                                    data-title="Đồng hồ trang trí ATT461"
                                                                    alt="Đồng hồ trang trí ATT461"
                                                                    href="san-pham/dong-ho-trang-tri-att461-200.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                        <!-- /.button-group -->
                                                    </div>
                                                    <!-- /.button-group -->
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="200"
                                                                data-name="san-pham"
                                                                data-title="Đồng hồ trang trí ATT461"
                                                                alt="Đồng hồ trang trí ATT461"
                                                                href="san-pham/dong-ho-trang-tri-att461-200.html">
                                                            Đồng hồ trang trí ATT461
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-new">400,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="199"
                                                            data-name="san-pham"
                                                            data-title="Chân nến thủy tinh AHE006"
                                                            alt="Chân nến thủy tinh AHE006"
                                                            href="san-pham/chan-nen-thuy-tinh-ahe006-199.html">
                                                        <img
                                                                src="upload/chan-nen-1-nac-ahe006-06-01-2017-12-16-00.png"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/chan-nen-1-nac-ahe006-06-01-2017-12-16-00.png"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <a
                                                                    class="btn-addToCart awe-button product-add-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(199,'[data-id=199] img')">
                                                                <i class="icon icon-shopping-bag"></i>
                                                            </a>
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(199,'[data-id=199] img')">
                                                                                <span>
                                                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                                </span>
                                                            </button>
                                                            <a
                                                                    data-id="199"
                                                                    data-name="san-pham"
                                                                    data-title="Chân nến thủy tinh AHE006"
                                                                    alt="Chân nến thủy tinh AHE006"
                                                                    href="san-pham/chan-nen-thuy-tinh-ahe006-199.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                        <!-- /.button-group -->
                                                    </div>
                                                    <!-- /.button-group -->
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="199"
                                                                data-name="san-pham"
                                                                data-title="Chân nến thủy tinh AHE006"
                                                                alt="Chân nến thủy tinh AHE006"
                                                                href="san-pham/chan-nen-thuy-tinh-ahe006-199.html">
                                                            Chân nến thủy tinh AHE006
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-new">1,500,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                </div>
                                <div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <span class="label label-sale">Sale</span>
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="198"
                                                            data-name="san-pham"
                                                            data-title="Chân nến kim loại ATT728"
                                                            alt="Chân nến kim loại ATT728"
                                                            href="san-pham/chan-nen-kim-loai-att728-198.html">
                                                        <img
                                                                src="upload/chan-nen-kim-loai-cao-capatt728-06-01-2017-12-16-20.png"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/chan-nen-kim-loai-cao-capatt728-06-01-2017-12-16-20.png"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <a
                                                                    class="btn-addToCart awe-button product-add-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(198,'[data-id=198] img')">
                                                                <i class="icon icon-shopping-bag"></i>
                                                            </a>
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(198,'[data-id=198] img')">
                                                                                <span>
                                                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                                </span>
                                                            </button>
                                                            <a
                                                                    data-id="198"
                                                                    data-name="san-pham"
                                                                    data-title="Chân nến kim loại ATT728"
                                                                    alt="Chân nến kim loại ATT728"
                                                                    href="san-pham/chan-nen-kim-loai-att728-198.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                        <!-- /.button-group -->
                                                    </div>
                                                    <!-- /.button-group -->
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="198"
                                                                data-name="san-pham"
                                                                data-title="Chân nến kim loại ATT728"
                                                                alt="Chân nến kim loại ATT728"
                                                                href="san-pham/chan-nen-kim-loai-att728-198.html">
                                                            Chân nến kim loại ATT728
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-old">1,400,000₫</span>
                                                        <span class="price-new">1,000,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <span class="label label-sale">Sale</span>
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="197"
                                                            data-name="san-pham"
                                                            data-title="Đèn chùm đặt bàn Iwannagohome AI0018"
                                                            alt="Đèn chùm đặt bàn Iwannagohome AI0018"
                                                            href="san-pham/den-chum-dat-ban-iwannagohome-ai0018-197.html">
                                                        <img
                                                                src="upload/den-chum-de-ban-ai0019-06-01-2017-12-16-45.png"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/den-chum-de-ban-ai0019-06-01-2017-12-16-45.png"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <a
                                                                    class="btn-addToCart awe-button product-add-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(197,'[data-id=197] img')">
                                                                <i class="icon icon-shopping-bag"></i>
                                                            </a>
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(197,'[data-id=197] img')">
                                                                                        <span>
                                                                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                                        </span>
                                                            </button>
                                                            <a
                                                                    data-id="197"
                                                                    data-name="san-pham"
                                                                    data-title="Đèn chùm đặt bàn Iwannagohome AI0018"
                                                                    alt="Đèn chùm đặt bàn Iwannagohome AI0018"
                                                                    href="san-pham/den-chum-dat-ban-iwannagohome-ai0018-197.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                        <!-- /.button-group -->
                                                    </div>
                                                    <!-- /.button-group -->
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="197"
                                                                data-name="san-pham"
                                                                data-title="Đèn chùm đặt bàn Iwannagohome AI0018"
                                                                alt="Đèn chùm đặt bàn Iwannagohome AI0018"
                                                                href="san-pham/den-chum-dat-ban-iwannagohome-ai0018-197.html">
                                                            Đèn chùm đặt bàn Iwannagohome AI0018
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-old">7,900,000₫</span>
                                                        <span class="price-new">7,500,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                </div>
                                <div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="196"
                                                            data-name="san-pham"
                                                            data-title="Vòi lavabo"
                                                            alt="Vòi lavabo"
                                                            href="san-pham/vo-i-lavabo-196.html">
                                                        <img
                                                                src="upload/voi-lavabo-a908go-copy-300x300-06-01-2017-12-11-20.png"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/voi-lavabo-a908go-copy-300x300-06-01-2017-12-11-20.png"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <a
                                                                    class="btn-addToCart awe-button product-add-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(196,'[data-id=196] img')">
                                                                <i class="icon icon-shopping-bag"></i>
                                                            </a>
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(196,'[data-id=196] img')">
                                                                                <span>
                                                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                                </span>
                                                            </button>
                                                            <a
                                                                    data-id="196"
                                                                    data-name="san-pham"
                                                                    data-title="Vòi lavabo"
                                                                    alt="Vòi lavabo"
                                                                    href="san-pham/vo-i-lavabo-196.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                        <!-- /.button-group -->
                                                    </div>
                                                    <!-- /.button-group -->
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="196"
                                                                data-name="san-pham"
                                                                data-title="Vòi lavabo"
                                                                alt="Vòi lavabo"
                                                                href="san-pham/vo-i-lavabo-196.html">
                                                            Vòi lavabo
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-new">50,000,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="195"
                                                            data-name="san-pham"
                                                            data-title="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                            alt="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                            href="san-pham/tay-sen-tron-johnson-suisse-caspian-ii-u00723-195.html">
                                                        <img
                                                                src="upload/tay-sen-johnson-suisse-a00723-1-06-01-2017-12-11-44.jpg"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/tay-sen-johnson-suisse-a00723-1-06-01-2017-12-11-44.jpg"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <a
                                                                    class="btn-addToCart awe-button product-add-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(195,'[data-id=195] img')">
                                                                <i class="icon icon-shopping-bag"></i>
                                                            </a>
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(195,'[data-id=195] img')">
                                                                                        <span>
                                                                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                                        </span>
                                                            </button>
                                                            <a
                                                                    data-id="195"
                                                                    data-name="san-pham"
                                                                    data-title="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                                    alt="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                                    href="san-pham/tay-sen-tron-johnson-suisse-caspian-ii-u00723-195.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                        <!-- /.button-group -->
                                                    </div>
                                                    <!-- /.button-group -->
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="195"
                                                                data-name="san-pham"
                                                                data-title="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                                alt="Tay sen tròn Johnson Suisse Caspian II U00723"
                                                                href="san-pham/tay-sen-tron-johnson-suisse-caspian-ii-u00723-195.html">
                                                            Tay sen tròn Johnson Suisse Caspian II U00723
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-new">600,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                </div>
                                <div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="194"
                                                            data-name="san-pham"
                                                            data-title="Tay sen Johnson Suisse Arctic II U00720"
                                                            alt="Tay sen Johnson Suisse Arctic II U00720"
                                                            href="san-pham/tay-sen-johnson-suisse-arctic-ii-u00720-194.html">
                                                        <img
                                                                src="upload/tay-sen-johnson-suisse-a00720-2-06-01-2017-12-12-03.jpg"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/tay-sen-johnson-suisse-a00720-2-06-01-2017-12-12-03.jpg"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <a
                                                                    class="btn-addToCart awe-button product-add-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(194,'[data-id=194] img')">
                                                                <i class="icon icon-shopping-bag"></i>
                                                            </a>
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(194,'[data-id=194] img')">
                                                                                <span>
                                                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                                </span>
                                                            </button>
                                                            <a
                                                                    data-id="194"
                                                                    data-name="san-pham"
                                                                    data-title="Tay sen Johnson Suisse Arctic II U00720"
                                                                    alt="Tay sen Johnson Suisse Arctic II U00720"
                                                                    href="san-pham/tay-sen-johnson-suisse-arctic-ii-u00720-194.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                        <!-- /.button-group -->
                                                    </div>
                                                    <!-- /.button-group -->
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="194"
                                                                data-name="san-pham"
                                                                data-title="Tay sen Johnson Suisse Arctic II U00720"
                                                                alt="Tay sen Johnson Suisse Arctic II U00720"
                                                                href="san-pham/tay-sen-johnson-suisse-arctic-ii-u00720-194.html">
                                                            Tay sen Johnson Suisse Arctic II U00720
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-new">1,500,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                    <div>
                                        <div class="item product-inner">
                                            <div class="item-inner transition">
                                                <span class="label label-sale">Sale</span>
                                                <div class="image">
                                                    <a
                                                            class="lt-image"
                                                            data-id="193"
                                                            data-name="san-pham"
                                                            data-title="Tay sen tròn Johnson Suisse Bering II U01072"
                                                            alt="Tay sen tròn Johnson Suisse Bering II U01072"
                                                            href="san-pham/tay-sen-tron-johnson-suisse-bering-ii-u01072-193.html">
                                                        <img
                                                                src="upload/tay-sen-a01072-2-600x600-06-01-2017-12-12-32.jpg"
                                                                class="img-1 product-featured-image"
                                                                alt="">
                                                        <img
                                                                src="upload/tay-sen-a01072-2-600x600-06-01-2017-12-12-32.jpg"
                                                                class="img-2 product-featured-image"
                                                                alt=""></a>
                                                    <div class="button-group">
                                                        <div class="button-group">
                                                            <a
                                                                    class="btn-addToCart awe-button product-add-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(193,'[data-id=193] img')">
                                                                <i class="icon icon-shopping-bag"></i>
                                                            </a>
                                                            <button
                                                                    type="button"
                                                                    class="button btn-cart btn-add-to-cart"
                                                                    title="Mua hàng"
                                                                    onclick="addCart(193,'[data-id=193] img')">
                                                                                        <span>
                                                                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                                        </span>
                                                            </button>
                                                            <a
                                                                    data-id="193"
                                                                    data-name="san-pham"
                                                                    data-title="Tay sen tròn Johnson Suisse Bering II U01072"
                                                                    alt="Tay sen tròn Johnson Suisse Bering II U01072"
                                                                    href="san-pham/tay-sen-tron-johnson-suisse-bering-ii-u01072-193.html"
                                                                    class="awe-button product-quick-view btn-quickview"
                                                                    data-toggle="modal"
                                                                    title="Xem chi tiết">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        </div>
                                                        <!-- /.button-group -->
                                                    </div>
                                                    <!-- /.button-group -->
                                                </div>
                                                <!-- /.image -->
                                                <div class="caption">
                                                    <h4>
                                                        <a
                                                                data-id="193"
                                                                data-name="san-pham"
                                                                data-title="Tay sen tròn Johnson Suisse Bering II U01072"
                                                                alt="Tay sen tròn Johnson Suisse Bering II U01072"
                                                                href="san-pham/tay-sen-tron-johnson-suisse-bering-ii-u01072-193.html">
                                                            Tay sen tròn Johnson Suisse Bering II U01072
                                                        </a>
                                                    </h4>
                                                    <p class="price">
                                                        <span class="price-old">10,000,000₫</span>
                                                        <span class="price-new">8,500,000₫</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /.item-inner -->
                                        </div>
                                        <!-- /.item -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="so-spotlight3">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="newsletter">
                            <h2>Đăng ký nhận
                                <span>tư vấn miễn phí</span>
                            </h2>
                            <p
                                    class="page-heading-sub"
                                    data-scroll-reveal="enter bottom and move 40px over 0.6s">
                                Bạn là khách hàng , lớn hay nhỏ, và muốn chúng tôi phục vụ , xin vui lòng gửi
                                cho chúng tôi một
                                <br>
                                email để info@flexshop.vn
                            </p>
                            <div class="block_content">
                                <form method="post">
                                    <div class="form-group required">
                                        <input
                                                type="email"
                                                name="txtemail"
                                                id="txtemail"
                                                value=""
                                                placeholder="Nhập email ..."
                                                class="form-control input-lg">
                                        <button type="submit" class="btn btn-default btn-lg">Đăng ký</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .so-spotlight3 {
                background: url("{{asset('views/template/src/newsletter-bgb09cb09c.jpg?v=180')}}") no-repeat center center;
            }
        </style>
        <section class="box_blog">
            <div class="container">
                <div class="modcontent">
                    <div class="header-title">
                        <h3 class="modtitle">
                            <span>Tin</span>&nbsp;<span>Mới</span>
                        </h3>
                    </div>
                    <div class="product-slider-5 product-thumb">
                        <div class="item item-blog">
                            <div class="blog_item_inner transition">
                                <a
                                        class="lt-image"
                                        data-id="206"
                                        data-name="tin-tuc"
                                        data-title="Hiệu quả không gian: đẳng cấp thiết kế nhà phố Camellia Garden"
                                        alt="Hiệu quả không gian: đẳng cấp thiết kế nhà phố Camellia Garden"
                                        href="tin-tuc/hieu-qua-khong-gian-dang-cap-thiet-ke-nha-pho-camellia-garden-206.html">
                                    <img
                                            src="upload/20160614140709-camellia-3_large-06-01-2017-15-11-37.jpg"
                                            alt="Hiệu quả không gian: đẳng cấp thiết kế nhà phố Camellia Garden"
                                            title="Hiệu quả không gian: đẳng cấp thiết kế nhà phố Camellia Garden"></a>
                                <div class="thongtin">
                                    <h4 class="blog_item_title">
                                        <a
                                                data-id="206"
                                                data-name="tin-tuc"
                                                data-title="Hiệu quả không gian: đẳng cấp thiết kế nhà phố Camellia Garden"
                                                alt="Hiệu quả không gian: đẳng cấp thiết kế nhà phố Camellia Garden"
                                                href="tin-tuc/hieu-qua-khong-gian-dang-cap-thiet-ke-nha-pho-camellia-garden-206.html">
                                            Hiệu quả không gian: đẳng cấp thiết kế nhà phố Camellia Garden
                                        </a>
                                    </h4>
                                    <div class="blog_item_motangan">Dự án nhà phố Camellia Garden mang đến gia chủ
                                        giải pháp thiết kế thông minh, khai thác công năng của các khu vực đến từng cm
                                        từ phòng khách...</div>
                                    <a
                                            data-id="206"
                                            data-name="tin-tuc"
                                            data-title="Hiệu quả không gian: đẳng cấp thiết kế nhà phố Camellia Garden"
                                            alt="Hiệu quả không gian: đẳng cấp thiết kế nhà phố Camellia Garden"
                                            href="tin-tuc/hieu-qua-khong-gian-dang-cap-thiet-ke-nha-pho-camellia-garden-206.html"
                                            class="blog_itemt_link">Xem thêm</a>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item item-blog">
                            <div class="blog_item_inner transition">
                                <a
                                        class="lt-image"
                                        data-id="205"
                                        data-name="tin-tuc"
                                        data-title="Không gian sống xanh giữa lòng thủ đô"
                                        alt="Không gian sống xanh giữa lòng thủ đô"
                                        href="tin-tuc/khong-gian-song-xanh-giua-long-thu-do-205.html">
                                    <img
                                            src="upload/s2_large-06-01-2017-15-21-20.jpg"
                                            alt="Không gian sống xanh giữa lòng thủ đô"
                                            title="Không gian sống xanh giữa lòng thủ đô"></a>
                                <div class="thongtin">
                                    <h4 class="blog_item_title">
                                        <a
                                                data-id="205"
                                                data-name="tin-tuc"
                                                data-title="Không gian sống xanh giữa lòng thủ đô"
                                                alt="Không gian sống xanh giữa lòng thủ đô"
                                                href="tin-tuc/khong-gian-song-xanh-giua-long-thu-do-205.html">
                                            Không gian sống xanh giữa lòng thủ đô
                                        </a>
                                    </h4>
                                    <div class="blog_item_motangan">Vị trí thuận lợi cho việc đi lại, không gian
                                        sống xanh mát gần công viên Bách Thảo và hồ Tây là điều kiện lý tưởng cho nơi an
                                        cư...</div>
                                    <a
                                            data-id="205"
                                            data-name="tin-tuc"
                                            data-title="Không gian sống xanh giữa lòng thủ đô"
                                            alt="Không gian sống xanh giữa lòng thủ đô"
                                            href="tin-tuc/khong-gian-song-xanh-giua-long-thu-do-205.html"
                                            class="blog_itemt_link">Xem thêm</a>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item item-blog">
                            <div class="blog_item_inner transition">
                                <a
                                        class="lt-image"
                                        data-id="204"
                                        data-name="tin-tuc"
                                        data-title="Phong cách hiện đại trong thiết kế nội thất là gì?"
                                        alt="Phong cách hiện đại trong thiết kế nội thất là gì?"
                                        href="tin-tuc/phong-cach-hien-dai-trong-thiet-ke-noi-that-la-gi--204.html">
                                    <img
                                            src="upload/phong-cach-hien-dai_large-06-01-2017-15-21-58.jpg"
                                            alt="Phong cách hiện đại trong thiết kế nội thất là gì?"
                                            title="Phong cách hiện đại trong thiết kế nội thất là gì?"></a>
                                <div class="thongtin">
                                    <h4 class="blog_item_title">
                                        <a
                                                data-id="204"
                                                data-name="tin-tuc"
                                                data-title="Phong cách hiện đại trong thiết kế nội thất là gì?"
                                                alt="Phong cách hiện đại trong thiết kế nội thất là gì?"
                                                href="tin-tuc/phong-cach-hien-dai-trong-thiet-ke-noi-that-la-gi--204.html">
                                            Phong cách hiện đại trong thiết kế nội thất là gì?
                                        </a>
                                    </h4>
                                    <div class="blog_item_motangan">Phong cách hiện đại là một thuật ngữ đã xuất
                                        hiện từ những thập niên 50 của thế kỉ 19 và ngày càng trở nên quen thuộc trong
                                        cuộc sống...</div>
                                    <a
                                            data-id="204"
                                            data-name="tin-tuc"
                                            data-title="Phong cách hiện đại trong thiết kế nội thất là gì?"
                                            alt="Phong cách hiện đại trong thiết kế nội thất là gì?"
                                            href="tin-tuc/phong-cach-hien-dai-trong-thiet-ke-noi-that-la-gi--204.html"
                                            class="blog_itemt_link">Xem thêm</a>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                        <div class="item item-blog">
                            <div class="blog_item_inner transition">
                                <a
                                        class="lt-image"
                                        data-id="203"
                                        data-name="tin-tuc"
                                        data-title="Người dùng chuộng nội thất sạch, chuẩn châu Âu"
                                        alt="Người dùng chuộng nội thất sạch, chuẩn châu Âu"
                                        href="tin-tuc/nguoi-dung-chuong-noi-that-sach-chuan-chau-au-203.html">
                                    <img
                                            src="upload/20160630150121-image007-5_large-06-01-2017-15-23-04.jpg"
                                            alt="Người dùng chuộng nội thất sạch, chuẩn châu Âu"
                                            title="Người dùng chuộng nội thất sạch, chuẩn châu Âu"></a>
                                <div class="thongtin">
                                    <h4 class="blog_item_title">
                                        <a
                                                data-id="203"
                                                data-name="tin-tuc"
                                                data-title="Người dùng chuộng nội thất sạch, chuẩn châu Âu"
                                                alt="Người dùng chuộng nội thất sạch, chuẩn châu Âu"
                                                href="tin-tuc/nguoi-dung-chuong-noi-that-sach-chuan-chau-au-203.html">
                                            Người dùng chuộng nội thất sạch, chuẩn châu Âu
                                        </a>
                                    </h4>
                                    <div class="blog_item_motangan">Nội thất chuẩn châu Âu là một khái niệm được
                                        nhắc đến nhiều trong thời gian gần đây, khi nhu cầu về một không gian sống tiện
                                        nghi, đẳng cấp...</div>
                                    <a
                                            data-id="203"
                                            data-name="tin-tuc"
                                            data-title="Người dùng chuộng nội thất sạch, chuẩn châu Âu"
                                            alt="Người dùng chuộng nội thất sạch, chuẩn châu Âu"
                                            href="tin-tuc/nguoi-dung-chuong-noi-that-sach-chuan-chau-au-203.html"
                                            class="blog_itemt_link">Xem thêm</a>
                                </div>
                            </div>
                            <!-- /.item-inner -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <section class="box_doitac">
            <div class="container">
                <div class="modcontent">
                    <div class="header-title" style="position:relative">
                        <h3 class="modtitle">
                                    <span>Đối
                                    </span>&nbsp;<span>tác</span>
                        </h3>
                        <!--<a href="/" class="pull-right moredoitac">Xem thêm</a>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="doitac_ct">
                        <div><img src="{{asset('views/template/src/doitac1b09cb09c.jpg?v=180')}}"/></div>
                        <div><img src="{{asset('views/template/src/doitac2b09cb09c.jpg?v=180')}}"/></div>
                        <div><img src="{{asset('views/template/src/doitac3b09cb09c.jpg?v=180')}}"/></div>
                        <div><img src="{{asset('views/template/src/doitac4b09cb09c.jpg?v=180')}}"/></div>
                        <div><img src="{{asset('views/template/src/doitac5b09cb09c.jpg?v=180')}}"/></div>
                        <div><img src="{{asset('views/template/src/doitac6b09cb09c.jpg?v=180')}}"/></div>
                        <div><img src="{{asset('views/template/src/doitac7b09cb09c.jpg?v=180')}}"/></div>
                        <div><img src="{{asset('views/template/src/doitac8b09cb09c.jpg?v=180')}}"/></div>
                        <div><img src="{{asset('views/template/src/doitac9b09cb09c.jpg?v=180')}}"/></div>
                        <div><img src="{{asset('views/template/src/doitac10b09cb09c.jpg?v=180')}}"/></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
    </div>
@endsection