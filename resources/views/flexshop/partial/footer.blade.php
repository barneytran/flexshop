<div class="footermap">
    <div class="container">
        <div class="so-maps">
            <div class="module google-map">
                <div class="modcontent clearfix">
                    <div class="contact">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3920.153939132416!2d106.68123281434984!3d10.722607592358052!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752fc942bad34b%3A0x8ee3d382f95bf077!2zNkIgUGjhuqFtIEjDuW5nLCBCw6xuaCBIxrBuZywgQsOsbmggQ2jDoW5oLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1530621522146"
                                width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footertop">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 social-box">
                <span>FOLLOW US</span>
                <ul class="social-block ">
                    <li class="facebook">
                        <a class="_blank" href="#" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li class="twitter">
                        <a class="_blank" href="#" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li class="rss">
                        <a class="_blank" href="#" target="_blank">
                            <i class="fa fa-rss"></i>
                        </a>
                    </li>
                    <li class="google_plus">
                        <a class="_blank" href="#" target="_blank">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </li>
                    <li class="pinterest">
                        <a class="_blank" href="#" target="_blank">
                            <i class="fa fa-pinterest"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6">
                <ul class="list-ft">
                    <li class="col-xs-6">
                        <a href="index.html">Trang chủ</a>
                    </li>
                    <li class="col-xs-6">
                        <a href="gioi-thieu.html" data-name="gioi-thieu" data-title="Giới thiệu">Giới thiệu</a>
                    </li>
                    <li class="col-xs-6">
                        <a href="{{route('listProjects')}}" data-name="san-pham" data-title="Sản phẩm">Dự án</a>
                    </li>
                    <li class="col-xs-6">
                        <a href="tin-tuc.html" data-name="tin-tuc" data-title="Tin tức">Thiết kế nội thất</a>
                    </li>
                    <li class="col-xs-6">
                        <a href="lien-he.html" data-name="lien-he" data-title="Liên hệ">Sản phẩm</a>
                    </li>
                    <li class="col-xs-6">
                        <a href="gio-hang.html" data-name="gio-hang" data-title="Giỏ hàng">Liên hệ</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3">
                <h3 class="title-f">Hỗ trợ</h3>
                <ul class="list-f">
                    <li>
                        <a href="#">Tư vấn thiết kế</a>
                    </li>
                    <li>
                        <a href="#">Chăm sóc khách hàng</a>
                    </li>
                    <li>
                        <a href="#">Kiểm tra đơn hàng</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <h3 class="title-f">Hướng dẫn</h3>
                <ul class="list-f">
                    <li>
                        <a href="#">Hướng dẫn mua hàng</a>
                    </li>
                    <li>
                        <a href="#">Giao nhận và thanh toán</a>
                    </li>
                    <li>
                        <a href="#">Đổi trả và bảo hành</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <h3 class="title-f">Chính sách</h3>
                <ul class="list-f">
                    <li>
                        <a href="#">Chính sách thanh toán</a>
                    </li>
                    <li>
                        <a href="#">Chính sách vận chuyển</a>
                    </li>
                    <li>
                        <a href="#">Chính sách bảo hành, đổi trả</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <h3 class="title-f">Tại sao chọn chúng tôi</h3>
                <ul class="list-f">
                    <li>
                        <a href="#">Đội ngũ chuyên nghiệp</a>
                    </li>
                    <li>
                        <a href="#">Giá cả hợp lý</a>
                    </li>
                    <li>
                        <a href="#">Đảm bảo tiến độ</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="copy text-center">
        © Bản quyền thuộc về Flexshop | Cung cấp bởi
        <a href="javacript:void(0)">Gaumeo</a>
    </div>
</div>