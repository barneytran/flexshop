<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//frontend flexshop
Route::get('/demo','FlexshopController@index')->name('homePage');
Route::group(['prefix'=>'demo'],function (){
    //project
    Route::get('/danh-sach-du-an','FlexshopController@getListProject')->name('listProjects');
    Route::get('/danh-sach-du-an/{id}/{cateSlug}','FlexshopController@cateProject')->name('cateListProject');
    Route::get('/du-an/{id}/{projectSlug}','FlexshopController@singleProject')->name('postProject');
    //product
    Route::get('/danh-sach-san-pham','FlexshopController@getListProduct')->name('getListProduct');
    Route::get('/danh-sach-san-pham/{id}/{cateSlug}','FlexshopController@cateProduct')->name('cateListProduct');
    Route::get('/danh-sach-san-pham/{id}/{cateSlug}/list','FlexshopController@listProduct')->name('listProduct');
    Route::get('/san-pham/{id}/{productSlug}','FlexshopController@singleProduct')->name('postProduct');
});


//*******************
Route::get('/', 'HomeController@index')->name('trangchu');
Route::get('/news/{id}/{newsSlug}', 'HomeController@singlepost')->name('postNews');
Route::get('/cate/{id}/{cateSlug}', 'HomeController@catepost')->name('catePost');
Route::get('/tags/{id}/{tagSlug}', 'HomeController@tagpost')->name('tagPost');
Route::get('/cateprod/{id}/{cateProdSlug}', 'HomeController@getCateProd')->name('cateProd');
Route::get('/prod/{id}/{prodSlug}', 'HomeController@getDetailProd')->name('hotProd');
Route::get('listprods','HomeController@getListProds')->name('listProds');
//backend
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::group(['prefix' => 'panel'], function () {
        //category
        Route::resource('category', 'CateController', ['except' => 'destroy']);
        Route::get('category/{idDelete}/destroy', 'CateController@destroy')->name('category.delete');
        //category product
        Route::resource('cateprod', 'CateProdController', ['except' => 'destroy']);
        Route::get('cateprod/{idDelete}/destroy', 'CateProdController@destroy')->name('cateprod.delete');
        //Prods
        Route::resource('prods', 'ProdsController', ['except' => 'destroy']);
        Route::get('prods/{idDelete}/destroy', 'ProdsController@destroy')->name('prods.delete');
        //article
        Route::resource('news', 'NewsController', ['except' => 'destroy']);
        Route::get('news/{idDelete}/destroy', 'NewsController@destroy')->name('news.delete');
        //slider
        Route::resource('slider','SliderController',['except' => 'destroy']);
        Route::get('slider/{idDelete}/destroy','SliderController@destroy')->name('slider.delete');
        //partner
        Route::resource('partner','PartnerController',['except' => 'destroy']);
        Route::get('partner/{idDelete}/destroy','PartnerController@destroy')->name('partner.delete');
        //tags
        Route::resource('tags', 'TagsController', ['except' => 'destroy']);
        Route::get('tags/{idDelete}/destroy', 'TagsController@destroy')->name('tags.delete');
        //user
        Route::resource('user', 'UserController');
    });
});

Auth::routes();

